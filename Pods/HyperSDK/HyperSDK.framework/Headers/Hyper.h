//
//  Hyper.h
//  HyperSDK
//
//  Copyright © Juspay Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Hyper : NSObject

//////////////////////////// Start of depreceated methods ////////////////////////

/**
 Response block for communicating between service to callee.
 
 @param status Status after execution of service.
 @param responseData Response from service once execution is complete.
 @param error Error object with details if any error has occurred.
 
 @since v0.1
 */
typedef void (^HyperResponseBlock)(int status, id _Nullable responseData, NSError * _Nullable error);

/**
 Response block for communicating logs between service to callee.
 
 @param logData Logs being passed from service.
 
 @since v0.1
 */
typedef void (^HyperLogBlock)(id _Nonnull logData);

/**
 Callback block for communicating between callee to service .
 
 @param data Data being passed for service.
 
 @since v0.1
 */
typedef void (^CallbackBlock)(NSDictionary* _Nonnull data);

/**
 Callback block to handle various callbacks/outputs from the SDK when process is called.
 
 @param data Response from service once execution is complete.
 */
typedef void (^HyperResponseHandler)(NSDictionary* _Nonnull data);

/**
 Callback block for communicating between callee to service .
 
 @param data Data being passed from service to callee.
 @param callback Callback to be triggered if required.
 
 @since v0.1
 */
typedef void (^HyperCommunicationBlock)(id _Nonnull data, CallbackBlock _Nonnull callback);

/**
 Hides bottom bar to provide more screen.
 
 @since v0.1
 */
@property (nonatomic) BOOL shouldHideBottomBarWhenPushed;
    
/**
 Data passed by calling app. Contains services to start - Mandatory to pass {"service":"service to be started"}.
 @warning `data` must have a valid service of type `{"service":"service to be started"}`.
 
 @since v0.1
 */
@property (nonatomic, strong, nonnull) NSDictionary *data;
    
///---------------------
/// @name Hyper entry points
///---------------------

/**
 Entry point for starting hyper.
 
 @param viewController Reference ViewController marked as starting point of view.
 @param data Data params to be passed for service - Mandatory to pass {"service":"service to be started"}.
 @param callback HyperResponse callback returns status and additional data required to calling process.
 @warning `data` must have a valid service of type `{"service":"service to be started"}`.
 
 @since v0.1
 */
- (void)startViewController:(nonnull UIViewController*)viewController data:(nonnull NSDictionary*)data callback:(nonnull HyperResponseBlock)callback;

/**
 Entry point for starting hyper.
 
 @param viewController Reference ViewController marked as starting point of view.
 @param data Data params to be passed for service - Mandatory to pass {"service":"service to be started"}.
 @param logs HyperLog callback returns logs from hyper modules.
 @param comm HyperCommunication used to pass data to and from calling service in current running service.
 @param callback HyperResponse callback returns status and additional data required to calling process.
 @warning `data` must have a valid service of type `{"service":"service to be started"}`.
 
 @since v0.1
 */
- (void)startViewController:(nonnull UIViewController*)viewController data:(nonnull NSDictionary*)data logCallback:(nullable HyperLogBlock)logs commBlock:(nullable HyperCommunicationBlock)comm callback:( nonnull HyperResponseBlock)callback;

/**
 Callback to be triggered by merchant.
 
 @return Callbackblock which contains a dictionary.
 
 @since v0.1
 */
- (CallbackBlock _Nullable )onMerchantEvent;

/**
 For updating assets and establishing connections.
 
 @since v0.1
 */
- (void)preFetch:(NSDictionary*_Nonnull)data __attribute__((deprecated("Replaced by +preFetch:")));

/**
 For clearing local cached data.
 
 @since v0.2
 */
+ (void)clearCache;
//////////////////////////// End of depreceated methods ////////////////////////

#pragma (All methods above are depreciated and will be removed in future releases)
#pragma mark - SDK Integration

/**
 Callback block for communicating between callee to service .
 
 @param data Data being passed for service.
 
 @since v0.4
 */
typedef void (^HyperEventsCallback)(NSDictionary* _Nonnull data);

/**
 Callback block to handle various callbacks/outputs from the SDK.
 
 @param data Response data from SDK once execution is complete.
 
 @since v0.4
 */
typedef void (^HyperSDKCallback)(NSDictionary<NSString*, id>* _Nullable data);

/**
 Custom loader to be shown in case of network calls.
 
 @since v0.1
 */
@property (nonatomic, strong, nullable) UIView *activityIndicator;

/**
 Merchant view will be visible on top of rendered micro app.
 
 @since v0.2
 */
@property (nonatomic, strong, nullable) UIView *merchantView;

/**
 Return the current version of SDK.
 
 @return Version number in string representation.
 
 @since v0.1
 */
+(NSString*_Nonnull)HyperSDKVersion;

/**
 For updating assets and establishing connections.
 
 @since v0.2
 */
+ (void)preFetch:(NSDictionary*_Nonnull)data;

/**
 Callback to be triggered by merchant.
 
 @return HyperEventsCallback to be triggered for passing data back.
 
 @since v0.4
 */
- (HyperEventsCallback _Nullable )merchantEvent;
    
///---------------------
/// @name Hyper entry points
///---------------------

/**
 For initiating Hyper engine.
 
 @param viewController Reference ViewController marked as starting point of view. Requires view to have a base UINavigationController.
 @param initiationPayload Payload required for starting up engine.
 @param callback Callback block to handle various callbacks or events triggered by SDK.
 
 @since v0.4
 */
- (void)initiate:(UIViewController * _Nonnull)viewController payload:(NSDictionary * _Nonnull)initiationPayload callback:(HyperSDKCallback _Nonnull)callback;

/**
 To perform an action.
 
 @param processPayload Payload required for an operation.
 
 @since v0.4
 */
- (void)process:(NSDictionary * _Nonnull)processPayload;

/**
 To be called after process in completed for clean up.
 
 @since v0.4
 */
- (void)terminate;

@end

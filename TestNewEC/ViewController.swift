//
//  ViewController.swift
//  TestNewEC
//
//  Created by Sachin Sharma on 07/10/19.
//  Copyright © 2019 Sachin Sharma. All rights reserved.
//

import UIKit
import HyperSDK

class ViewController: UIViewController {
    var hyper = Hyper();

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func prefetch(_ sender: Any) {
        Hyper.preFetch([
            "clientId":"swiggy_ios",
            "betaAssets":"true"
        ])
    }
    
    @IBAction func initiate(_ sender: Any) {
        let params : [AnyHashable : Any] = [
            "requestId" : UUID.init().uuidString,
             "service" : "in.juspay.ec",
             "payload"  : [
                "environment": "sandbox",
                "merchantId": "merchantId",
                "betaAssets" : "true",
                "clientId": "swiggy_ios",
                "customerId": "customerId",
                "action":"initiate"
            ]
        ]
        hyper.initiate(self, payload: params, callback: { (data) in
           if data != nil {
              let event = data?["event"] as! String
               if event != nil && event == "process_result"{
               //Handle process result here
               }else{
                            
               }
            }else{
                        
            }
        })
    }
    
    @IBAction func process(_ sender: Any) {
        let params : [AnyHashable : Any] = [
            "requestId" : UUID.init().uuidString,
             "service" : "in.juspay.ec",
             "payload"  : [
                "paymentMethod" : "NB_HDFC",
                "orderId" : "orderId",
                "endUrls" : ["juspay.in"],
                "action":"nbTxn"
            ]
        ]
        hyper.process(params)
    }
}

